package atraccioFira;
/**
 * Clase que contiene el Main.
 * @author guillermo
 *
 */
public class AtraccioFires {

	public static void main(String[] args) {
		Sensor sensor = new Sensor();
		PortaEntrada entrada = new PortaEntrada();
		int height = 0;
		for(int i = 1; i < 21; i++) {
			height = sensor.obtenirAlcada();
			System.out.println("Usuari " + i + ":");
			System.out.print("\tAlçada llegida = " + height + " cms. ");
			entrada.comprovar(height);
		}

	}

}
