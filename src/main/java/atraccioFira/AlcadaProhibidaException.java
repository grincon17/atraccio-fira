package atraccioFira;
/**
 * Excepción personalizada
 * @author guillermo
 *
 */
public class AlcadaProhibidaException extends Exception {
	
	private int errorCode;
	
	public AlcadaProhibidaException(int errorCode) {
		super();
		this.errorCode = errorCode;
	}
	
	@Override
	public String getMessage() {
		final int MIN_HEIGHT = 150;
		final int MAX_HEIGHT = 190;
		String msg = "";
		
		if(errorCode < MIN_HEIGHT) {
			msg = "ERROR: No arriba a la alçada minima de " + MIN_HEIGHT;
		} else {
			msg = "ERROR: alçada màxima de " + MAX_HEIGHT + " sobrepassada";
		}
	
		return msg;
	}

}
