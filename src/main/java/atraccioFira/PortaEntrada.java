package atraccioFira;
/**
 * Clase que comprueba y valida la altura del usuario 
 * @author guillermo
 *
 */
public class PortaEntrada {
	
	private final int MIN_HEIGHT = 150;
	private final int MAX_HEIGHT = 190;

	/**
	 * 
	 * Metodo que recibe una altura y la valida. Si no cumple el requisito lanza una excepción
	 * @param height la altura a comprobar
	 */
	public void comprovar(int height) {
		
		try {
			if(height >= MIN_HEIGHT && height <= MAX_HEIGHT) {
				System.out.println("OK! Obrint portes :·)");
			} else {
				throw new AlcadaProhibidaException(height);
			} 
		} catch(AlcadaProhibidaException ape) {
			System.out.println(ape.getMessage());	
		}
		
	}
}
